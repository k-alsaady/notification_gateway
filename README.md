
# Gem for push notifications: 

This gem allow you to easily push notification using different notification providers like pubnub ..etc and use different notification provider for each environment.

  
# Installation:

- Using plain ruby:
  
   gem install notification_gateway 

- Using Rails:

   add gem "notification_gateway" to your Gemfile


# Usage:

  - pubnub notification:

### Example1 (using same notification provider for all environments):
      
```ruby

pubnub_gateway = NotificationGateway.new gateway:  :pubnub, subscribe_key: "demo", publish_key: "demo", logger_path: "path_to_pubnub.log"

pubnub_gateway.publish channel: "channel_name", message: "put your message here"

```

### Example2 (using different notification provider for each rails environment):


```ruby
# in config/environemnts/development.rb
config.notification_gateway = :pubnub

#in config/environemnts/dev.rb add this line 
config.notification_gateway = :action_cable
```

in your code create new instance of the NotificationGateway and use it you need to pass all required configurations 
for all different notification providers you are using, i would suggest to create a global valirable 
inside config/initializers/notification_gateway.rb like this:


```ruby
$notification_gateway = NotificationGateway.new subscribe_key: "demo", publish_key: "demo", logger_path: "path_to_pubnub.log"
```
        
use that global valirable anywhere you want to send notification

```ruby
$notification_gateway.publish channel: "channel_name", message: "put your message here"
```

* Supported notifications:

    - pubnub
    - rails action_cabe
    
* TODO:

  - Integrate firebase