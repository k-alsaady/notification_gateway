
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "notification_gateway/version"

Gem::Specification.new do |spec|
  spec.name          = "notification_gateway"
  spec.version       = NotificationGateway::VERSION
  spec.authors       = ["Khalid Alsaadi"]
  spec.email         = ["kh.alsaady@gmail.com"]

  spec.summary       = %q{Simple gem to push notification using different push notification providers like (pubnub)}
  spec.description   = %q{Simple gem to push notification using different push notification providers like (pubnub)}
  spec.homepage      = "https://bitbucket.org/k-alsaady/notification_gateway/src/master/"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "https://rubygems.org"

    spec.metadata["homepage_uri"] = spec.homepage
    spec.metadata["source_code_uri"] = "https://bitbucket.org/k-alsaady/notification_gateway/src/master/"
    # spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  
  # ------------ Dependencies ----------------- #
  
  spec.add_dependency "activesupport", ">= 5.2.3"
  spec.add_dependency "pubnub", "~> 4.1.2"
  spec.add_dependency "actioncable", "~> 5.2", ">= 5.2.3"
  
  # ------------ Development dependencies ----------------- #
  
  spec.add_development_dependency "bundler", "~> 2.0.2"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
  spec.add_development_dependency "pry"

end
