require "active_support"
require "pubnub"
require "action_cable"
require "notification_gateway/pubnub"
require "notification_gateway/action_cable_gateway"

class NotificationGateway
  
  attr_accessor :gateway, :options, :notifier

  def initialize **options
    @gateway   = set_gateway options
    @options   = options
    @notifier  = set_notifier

    raise 'Missing gateway!' unless @gateway
    raise 'actioncable gateway supported only with rails projects' if @gateway == :action_cable && !defined?(Rails) 
  end

  def publish message:, channel:
    notifier.publish message: message, channel: channel
  end

  def set_gateway options
    if options[:gateway] 
      options[:gateway]
    elsif defined? Rails
      Rails.configuration.respond_to?(:notification_gateway) ? Rails.configuration.notification_gateway : nil
    end
  end

  def set_notifier
    case gateway
    when :pubnub 
      NotificationGateway::Pubnub.new(options)
    when :action_cable
      NotificationGateway::ActionCableGateway
    end
  end

end
