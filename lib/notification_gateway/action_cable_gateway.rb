module NotificationGateway::ActionCableGateway
  
  def self.publish channel:, message:
    ActionCable.server.broadcast channel, message
  end

end