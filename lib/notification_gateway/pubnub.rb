class NotificationGateway::Pubnub
  attr_accessor :options, :subscribe_key, :publish_key, :pubnub, :logger, :channel, :message

  def initialize **options
    @options       = options
    @subscribe_key = options[:subscribe_key]
    @publish_key   = options[:publish_key]
    @logger        = set_logger

    raise "publish_key is missing"   unless publish_key
    raise "subscribe_key is missing" unless subscribe_key
    
    @pubnub = Pubnub.new(
      subscribe_key: @subscribe_key,
      publish_key:   @publish_key,
      logger: @logger
    )
  end

  def publish channel:, message:  
    pubnub.publish(
      channel: channel,
      message: message
    ) {|envelope| }# puts envelope.status}        
  end

  def set_logger
    options[:logger] || ActiveSupport::Logger.new(options[:logger_path]) 
  end

end